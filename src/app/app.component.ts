import { Component } from '@angular/core';

@Component({
  selector: 'semaforo-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  public semaforoVehiculos = 'verde';
  public semaforoPeatones = 'rojo';
  public semaforosAndando = false;
  public textoBoton = 'Iniciar semáforos';
  private intervalo;

  public getSemaforoVehiculos(color: string): boolean {
    return this.semaforoVehiculos === color;
  }

  public getSemaforoPeatones(color: string): boolean {
    return this.semaforoPeatones === color;
  }

  public toggleSemaforos(): void {
    if (this.semaforosAndando) {
      this.textoBoton = 'Iniciar semáforos';
      this.pausarSemaforos();
    } else {
      this.textoBoton = 'Pausar semáforos';
      this.arrancarSemaforos();
    }
    this.semaforosAndando = !this.semaforosAndando;
  }

  private arrancarSemaforos(): void {
    this.intervalo = setInterval(() => {
      if (this.semaforoVehiculos === 'verde') {
        this.semaforoVehiculos = 'amarillo';
        this.semaforoPeatones = 'rojo';
      } else if (this.semaforoVehiculos === 'amarillo') {
        this.semaforoVehiculos = 'rojo';
        this.semaforoPeatones = 'verde';
      } else {
        this.semaforoVehiculos = 'verde';
        this.semaforoPeatones = 'rojo';
      }
    }, 1000);
  }

  private pausarSemaforos(): void {
    clearInterval(this.intervalo);
  }
}
